package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._

case class Member(
    id: Pk[Long] = NotAssigned,
    name: String
)

object Member {

  /**
   * Parse a Member from a ResultSet
   */
  val simple = {
    get[Pk[Long]]("member.id") ~
    get[String]("member.name") map {
      case id~name => Member(id, name)
    }
  }

  /**
   * Construct the Map[String,String] needed to fill a select options set.
   */
  def options: Seq[(String,String)] = DB.withConnection { implicit connection =>
    SQL("select * from member order by name").as(Member.simple *).map(c => c.id.toString -> c.name)
  }
}
