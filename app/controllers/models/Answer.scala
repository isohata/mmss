package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._

case class Answer(
    id: Pk[Long] = NotAssigned,
    topicId: Int,
    memberId: Int,
    answer: String
)

object Answer {

  // -- Parsers

  /**
   * Parse a Answer from a ResultSet
   */
  val simple = {
    get[Pk[Long]]("answer.id") ~
    get[Int]("answer.topic_id") ~
    get[Int]("answer.member_id") ~
    get[String]("answer.answer") map {
      case id~topic_id~member_id~answer => Answer(id, topic_id, member_id, answer)
    }
  }

//  /**
//   * Parse a (Answer,Topic) from a ResultSet
//   */
//  val withTopic = Answer.simple ~ (Topic.simple ?) map {
//    case answer~topic => (answer,topic)
//  }

  // -- Queries

  /**
   * Retrieve a answer from the id.
   */
  def findById(id: Long): Option[Answer] = {
    DB.withConnection { implicit connection =>
      SQL("select * from answer where id = {id}").on('id -> id).as(Answer.simple.singleOpt)
    }
  }

//  /**
//   * Return a page of (Answer,Topic,Member).
//   *
//   * @param topicId topic id
//   * @param page Page to display
//   * @param pageSize Number of topic answer per page
//   * @param orderBy Topic property used for sorting
//   * @param filter Filter applied on the name column
//   */
//  def list(topicId: Pk[Long], page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%"): Seq[(Answer)] = {
//
//    val offest = pageSize * page
//
//    DB.withConnection { implicit connection =>
//
//      val answers = SQL(
//        """
//          select * from answer
//          left join topic on answer.topic_id = topic.id
//          left join member on answer.member_id = member.id
//          where answer.topic_id = {topicId}
//          order by {orderBy} nulls last
//          limit {pageSize} offset {offset}
//        """
//      ).on(
//        'pageSize -> pageSize,
//        'offset -> offest,
//        'filter -> filter,
//        'orderBy -> orderBy
//      ).as(Answer.simple * )
//
////      val totalRows = SQL(
////        """
////          select count(*) from answer
////          where answer.topic_id = {topicId}
////        """
////      ).on(
////        'filter -> filter
////      ).as(scalar[Long].single)
//
//      //Page(answers, page, offest, totalRows)
//      answers
//
//    }
//
//  }

  /**
   * Update a answer.
   *
   * @param id The answer id
   * @param answer The answer values.
   */
  def update(id: Long, answer: Answer) = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          update answer
          set topic_id = {topic_id}, member_id = {member_id}, answer = {answer}
          where id = {id}
        """
      ).on(
        'id -> id,
        'answer -> answer.answer,
        'topic_id -> answer.topicId,
        'member_id -> answer.memberId
      ).executeUpdate()
    }
  }

  /**
   * Insert a new answer.
   *
   * @param answer The answer values.
   */
  def insert(answer: Answer) = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          insert into answer values (
            (select next value for answer_seq),
            {topic_id}, {member_id}, {answer}
          )
        """
      ).on(
        'answer -> answer.answer,
        'topic_id -> answer.topicId,
        'member_id -> answer.memberId
      ).executeUpdate()
    }
  }

  /**
   * Delete a answer.
   *
   * @param id Id of the answer to delete.
   */
  def delete(id: Long) = {
    DB.withConnection { implicit connection =>
      SQL("delete from answer where id = {id}").on('id -> id).executeUpdate()
    }
  }

    /**
   * get list
   *
   * @param id Id of the topic to get
   */
  def listByTopicId(topicId: Long) = {
    DB.withConnection { implicit connection =>
      SQL("select * from answer where topic_id = {topicId}").on('topicId -> topicId)
        .as(Answer.simple *)
        //.as(long("id") ~ long("topic_id") ~ long("member_id") ~ str("answer") map(flatten) *)
    }
  }

}
