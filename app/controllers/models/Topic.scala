package models

import play.api.db._
import play.api.Play.current
import anorm._
import anorm.SqlParser._
import models.Answer._

case class Topic(
    id: Pk[Long] = NotAssigned,
    title: String,
    Answers: Seq[Answer]
)

object Topic {

  // -- Parsers

  /**
   * Parse a Topic from a ResultSet
   */
  val simple = {
    get[Pk[Long]]("topic.id") ~
    get[String]("topic.title") map {
      case id~title => Topic(id, title, Answer.listByTopicId(id.get))
    }
  }

//  /**
//   * Parse a (Topic,Answers) from a ResultSet
//   */
//  val withAnswers = Topic.simple ~ Answer.list(?) map {
//    case topic => (topic)
//  }

  // -- Queries

  /**
   * Retrieve a topic from the id.
   */
  def findById(id: Long): Option[Topic] = {
    DB.withConnection { implicit connection =>
      SQL("select * from topic where id = {id}").on('id -> id).as(Topic.simple.singleOpt)
    }
  }

  /**
   * Return a page of (Topic,Member).
   *
   * @param page Page to display
   * @param pageSize Number of topics per page
   * @param orderBy Topic property used for sorting
   * @param filter Filter applied on the name column
   */
  def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%"): Page[(Topic)] = {

    val offest = pageSize * page

    DB.withConnection { implicit connection =>

      val topics = SQL(
        """
          select * from topic
          where topic.title like {filter}
          order by {orderBy} nulls last
          limit {pageSize} offset {offset}
        """
      ).on(
        'pageSize -> pageSize,
        'offset -> offest,
        'filter -> filter,
        'orderBy -> orderBy
      //).as(Topic.withAnswers *)
        ).as(Topic.simple *)

      val totalRows = SQL(
        """
          select count(*) from topic
          where topic.title like {filter}
        """
      ).on(
        'filter -> filter
      ).as(scalar[Long].single)

      Page(topics, page, offest, totalRows)

    }

  }

  /**
   * Update a topic.
   *
   * @param id The topic id
   * @param topic The topic values.
   */
  def update(id: Long, topic: Topic) = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          update topic
          set title = {title}
          where id = {id}
        """
      ).on(
        'id -> id,
        'title -> topic.title
      ).executeUpdate()
    }
  }

  /**
   * Insert a new topic.
   *
   * @param topic The topic values.
   */
  def insert(topic: Topic) = {
    DB.withConnection { implicit connection =>
      SQL(
        """
          insert into topic values (
            (select next value for topic_seq),
            {title}
          )
        """
      ).on(
        'title -> topic.title
      ).executeUpdate()
    }
  }

  /**
   * Delete a topic.
   *
   * @param id Id of the topic to delete.
   */
  def delete(id: Long) = {
    DB.withConnection { implicit connection =>
      SQL("delete from topic where id = {id}").on('id -> id).executeUpdate()
    }
  }

}
