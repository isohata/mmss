package controllers

import play.api._
import play.api.mvc._
import play.api.data.Form
import play.api.data.Forms._
import anorm._
import models._
import views._

object Application extends Controller {

  /**
   * This result directly redirect to the application home.
   */
  val Home = Redirect(routes.Application.list(0, 2, ""))

  /**
   * Describe the topic form (used in both edit and create screens).
   */
  val topicForm = Form(
    mapping(
      "id" -> ignored(NotAssigned:Pk[Long]),
      "title" -> nonEmptyText,
      "answers" -> seq(
          mapping( //member毎にこのデータが複数ある
              "id" -> ignored(NotAssigned:Pk[Long]),
              "topicId" -> number,
              "memberId" -> number,
              "answer" -> text
          )(Answer.apply)(Answer.unapply)
      )
    )(Topic.apply)(Topic.unapply)
  )


  // -- Actions

  /**
   * Handle default path requests, redirect to topics list
   */
  def index = Action { Home }

  /**
   * Display the paginated list of topics.
   *
   * @param page Current page number (starts from 0)
   * @param orderBy Column to be sorted
   * @param filter Filter applied on topic names
   */
  def list(page: Int, orderBy: Int, filter: String) = Action { implicit request =>
    Ok(html.list(
      Topic.list(page = page, orderBy = orderBy, filter = ("%"+filter+"%")),
      orderBy, filter
    ))
  }

  /**
   * Display the 'edit form' of a existing Topic.
   *
   * @param id Id of the topic to edit
   */
  def edit(id: Long) = Action {
    Topic.findById(id).map { topic =>
      Ok(html.editForm(id, topicForm.fill(topic)))
    }.getOrElse(NotFound)
  }

  /**
   * Handle the 'edit form' submission
   *
   * @param id Id of the topic to edit
   */
  def update(id: Long) = Action { implicit request =>
    topicForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.editForm(id, formWithErrors)),
      topic => {
        Topic.update(id, topic)
        Home.flashing("success" -> "Topic %s has been updated".format(topic.title))
      }
    )
  }

  /**
   * Display the 'new topic form'.
   */
  def create = Action {
    Ok(html.createForm(topicForm))
  }

  /**
   * Handle the 'new topic form' submission.
   */
  def save = Action { implicit request =>
    topicForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.createForm(formWithErrors)),
      topic => {
        Topic.insert(topic)
        Home.flashing("success" -> "Topic %s has been created".format(topic.title))
      }
    )
  }

  /**
   * Handle topic deletion.
   */
  def delete(id: Long) = Action {
    Topic.delete(id)
    Home.flashing("success" -> "Topic has been deleted")
  }

}
