# --- Sample dataset

# --- !Ups

insert into member (id,name) values (  1,'user1');
insert into member (id,name) values (  2,'user2');

# --- !Downs

delete from topic;
delete from member;
