# --- First database schema

# --- !Ups

set ignorecase true;

create table member (
  id                        bigint not null,
  name                      varchar(255) not null,
  constraint pk_member primary key (id))
;

create table topic (
  id                        bigint not null,
  title                     varchar(255) not null,
  constraint pk_topic primary key (id))
;

create table answer (
  id                        bigint not null,
  answer                    varchar(255),
  topic_id                  bigint,
  member_id                 bigint,
  constraint pk_answer primary key (id))
;

create sequence member_seq start with 1000;

create sequence topic_seq start with 1000;

create sequence answer_seq start with 1000;

alter table answer add constraint fk_anser_member_1 foreign key (member_id) references member (id) on delete restrict on update restrict;
alter table answer add constraint fk_anser_topic_1 foreign key (topic_id) references member (id) on delete restrict on update restrict;
create index ix_answer_member_1 on answer (member_id);
create index ix_answer_topic_1 on answer (topic_id);


# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists member;

drop table if exists topic;

drop table if exists answer;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists member_seq;

drop sequence if exists topic_seq;

drop sequence if exists answer_seq;

